// var sortable = null;
var states = []

$(document).ready(function(){
  $($('.nav-tabs li')[0]).attr('title', $('.nav-tabs li div span').text())
  // console.log($($('.nav-tabs li')[0]).attr('title'));

  if (navigator.platform.indexOf('Win') != -1 || navigator.platform.indexOf('Windows') != -1){
    $('.title-control').css('display', 'flex');
  }

  $('#btn-close-download-bar').click(function(){
    var sizeDownloadBar = ($('.download-bar').width()-35);
    var sumList = 0;
    for (var i = 0; i < $('.download-item').length + 1; i++) {
      sumList += ($('.download-item').width() + 0);
    }
    console.log(sumList);
    console.log(sizeDownloadBar);
    if (sumList >= sizeDownloadBar){
      console.log('ok');
      $($('.download-item')[$('.download-item').length-1]).remove();
    }
    $('.download-list').prepend('<li class="download-item dropup"><progress value="80" max="100"></progress><div class="item-title">111firefox-50.0.1.tar.bz2dadsaads</div><div class="item-size">9.5/54.2 MB, Paused</div><div class="download-option dropdown-toggle" data-toggle="dropdown"><i class="material-icons">keyboard_arrow_up</i></div><ul class="dropdown-menu dropdown-menu-right list-menu"><li><a href="#">Pause</a></li><li><a href="#">Show in folder</a></li><li class="divider"></li><li><a href="#">Download manager</a></li><li class="divider"></li><li><a href="#">Cancel</a></li></ul></li>')
  })

  const {remote} = require('electron');
  const {BrowserWindow} = remote;
  const win = BrowserWindow.getFocusedWindow();

  $('#btn-minimize').click(function(){
    win.minimize();
  })

  $('#btn-toggleMaximize').click(function(){
    // if (win.isMaximizable()){
    //   win.restore();
    //   win.setMaximizable(false)
    // }else {
    //   win.maximize();
    //   win.setMaximizable(true)
    // }
  })

  $('#btn-exit').click(function(){
    win.close();
  })

  var boolean_permission = false

  if (!localStorage.getItem('zoomLevel') || localStorage.getItem('zoomLevel') == 'null'){
    localStorage.setItem('zoomLevel', 7);
  }
  sessionStorage.setItem('tempZoomLevel', localStorage.getItem('zoomLevel'));

  var _id = guid();
  $('.active').attr('id', _id);
  $('.active div img').attr('id', ("favicon"+_id));
  $('.active div span').attr('id', ("title"+_id));
  $($('.webview webview')[0]).attr('id', $('.active').attr('id'))

  var el = document.getElementById('nav-tabs');
  var sortable = Sortable.create(el, {onStart: function(e){
    setTimeout(function () {
      $('.active').removeClass('active');
      $($('.tab-container .nav-tabs li')[e.oldIndex]).addClass('active');

      var id = '#'+$('.active').attr('id');

      var url = $('.webview').find(id)[0].getURL();

      if (url.indexOf(start_page) != -1){
        $('#address').val("");
      }else {
        if ($('#address').val() != url){
          $('#address').val(url);
        }
      }

      var listWebView = $('.webview webview');
      for (var i = 0; i < listWebView.length-1; i++) {
        $(listWebView[i]).css('height', 0)
      }

      setTimeout(function () {
        $('.webview').find(id).css('height', '100%')
      }, 100);
    }, 100);
  }, onEnd: function(e){
    $($('.tab-container .nav-tabs li')[e.oldIndex]).mouseleave(function(event){
      if (event.clientY > 70){
        if ($('.nav-tabs li').length-1 > 0){
          var id = '#'+$('.active').attr('id');
          target_link = $('.webview').find(id)[0].getURL();
          openInNewWindow();
          closeTab($('.active').attr('id'), 'new-window');
          $(this).unbind()
        }
      }
    })
    changeTab()
  }});

  //---------Handling button menu---------------
  $('#btn-menu').click(function(){
    $('.address-bar').addClass('open')
  })

  $(document).click(function(e){
    if (!$(e.target).is('#btn-zoom-in') && !$(e.target).is('#btn-zoom-out') && !$(e.target).is('#btn-menu') && !$(e.target).is('#btn-default-level') && !$(e.target).is('#btn-toggleNight') && !$(e.target).is('#btn-block-ads') && !$(e.target).is('#btn-toggle')){
      $('.address-bar').removeClass('open')
      localStorage.setItem('zoomLevel', sessionStorage.getItem('tempZoomLevel'))
    }
  })

  $('webview').focus(function(){
    $('.address-bar').removeClass('open')
    localStorage.setItem('zoomLevel', sessionStorage.getItem('tempZoomLevel'))
  })
  //-----------------End------------------------

  // $('.btn-add').mouseover(function(){
  //   $('.btn-add i').css('color', 'white');
  // })
  //
  // $('.btn-add').mouseout(function(){
  //   $('.btn-add i').css('color', '#C0C0C0');
  // })
  //
  // $('.btn-trash').mouseover(function(){
  //   $('.btn-trash i').css('color', 'white');
  // })
  //
  // $('.btn-trash').mouseout(function(){
  //   $('.btn-trash i').css('color', '#C0C0C0');
  // })

  $('#btn-back').mouseover(function(){
    if ($(this).hasClass('disabled')){
      $(this).css('border', '1px solid transparent');
    }else {
      $(this).css('border', '1px solid #ccc');
      $(this).css('border-radius', '0px');
    }
  })

  $('#btn-forward').mouseover(function(){
    if ($(this).hasClass('disabled')){
      $(this).css('border', '1px solid transparent');
    }else {
      $(this).css('border', '1px solid #ccc');
      $(this).css('border-radius', '0px');
    }
  })

  $('#btn-back').mouseout(function(){
    $(this).css('border', '1px solid transparent');
  })

  $('#btn-forward').mouseout(function(){
    $(this).css('border', '1px solid transparent');
  })

  $('#address').focus(function(){
    $(this).select();
    $('.find-box').css('z-index', '0');
  })

  $('#address').blur(function(){
    $('.find-box').css('z-index', '9');
    setTimeout(function () {
      $('#address-suggest li').remove()
    }, 200);
  })

  $('#address').keyup(function(e){
    console.log(e.keyCode);

    if (e.keyCode == 40 && $('#address-suggest li').length > 0){
      var current_selected = $('#address-suggest .selected').index()
      console.log(current_selected);

      if (current_selected < $('#address-suggest li').length - 1){
        $($('#address-suggest li')[current_selected]).removeClass('selected')
        $($('#address-suggest li')[current_selected+1]).addClass('selected')
      }

      $('#address').val($('.selected').text().replace(" - Google search", ""));
    }else if (e.keyCode == 38 && $('#address-suggest li').length > 0) {
      var current_selected = $('#address-suggest .selected').index()
      console.log(current_selected);

      if (current_selected > 0){
        $($('#address-suggest li')[current_selected]).removeClass('selected')
        $($('#address-suggest li')[current_selected-1]).addClass('selected')
        $('#address').val($('.selected').text().replace(" - Google search", ""));
      }else {
        $($('#address-suggest li')[current_selected]).removeClass('selected')
      }
    }else if (e.keyCode == 13){
      for (var i = 0; i < $('#address-suggest li').length; i++) {
        if ($($('#address-suggest li')[i]).hasClass('selected')) {
          var url = $($('#address-suggest li')[i]).text()
          if (url.indexOf(' - Google search')) {
            url = url.replace(' - Google search', '');
          }
          document.getElementsByTagName('webview')[$('.active').index()].loadURL('https://www.google.com.vn/?gws_rd=ssl#q='+url.replace(/ /g, "%20"))
          break;
        }else {
          var url = $('#address').val();

          if (url.indexOf('http://') == -1 && url.indexOf('https://') == -1){
            url = 'http://'+url;
          }

          $('.find-box').css('display', 'none');
          if (checkUrl(url)){
            document.getElementsByTagName('webview')[$('.active').index()].loadURL(url)
          }else {
            document.getElementsByTagName('webview')[$('.active').index()].loadURL('https://www.google.com.vn/?gws_rd=ssl#q='+url.substring(7, url.length))
          }
          break;
        }
      }
      $('#address').blur();
    }else if (e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40 && e.keyCode != 13 && e.keyCode != 16 && e.keyCode != 17 && e.keyCode != 20 && e.keyCode != 27 && e.keyCode != 112 && e.keyCode != 113 && e.keyCode != 114 && e.keyCode != 115 && e.keyCode != 116 && e.keyCode != 117 && e.keyCode != 118 && e.keyCode != 119 && e.keyCode != 120 && e.keyCode != 121 && e.keyCode != 122 && e.keyCode != 123 && e.keyCode != 18 && e.keyCode != 91) {
      if ($(this).val().length == 0){
        $('#address-suggest li').remove()
      }else {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
             // Action to be performed when the document is read;
            //  console.log(this.responseText.replace(/\\/g, "").replace('/*""*/', "").replace(/"/g, ""));
            $('#address-suggest li').remove()
            var suggestions = JSON.parse(this.responseText);
            var first = suggestions[0]
            $('#address-suggest').append("<li class='list-group-item'>"+suggestions[0]+" - Google search</li>")
            // $($('#address-suggest li')[0]).addClass('selected')
            for (var i = 0; i < suggestions[1].length; i++) {
              if (first != suggestions[1][i][0].replace("<b>", "").replace("</b>", "")){
                $('#address-suggest').append("<li class='list-group-item'>"+suggestions[1][i][0]+"</li>")
              }

              console.log(suggestions[1][i][0].replace("<b>", "").replace("</b>", ""));
            }
            console.log(JSON.parse(this.responseText));

            $('#address-suggest li').click(function(){
              console.log($(this).text());
              document.getElementsByTagName('webview')[$('.active').index()].loadURL('https://www.google.com.vn/?gws_rd=ssl#q='+$(this).text().replace(/ /g, "%20"))
            })
          }
        };
        xhttp.open("GET", "https://clients1.google.com/complete/search?q="+$(this).val()+"&nolabels=t&client=psy&ds=&_=1481449431283", true);
        xhttp.send();
      }
    }


  });

  $('#btn-back').click(function(){
    goBack()
  })

  $('#btn-forward').click(function(){
    goForward()
  })

  $('#btn-refresh').click(function(){
    doRefresh()
  })

  $('#btn-home').click(function(){
    goHome()
  })

  $('#btn-favorite').click(function(){
    if ($('#btn-favorite i').text().indexOf('bookmark_border') != -1){
      $('#btn-favorite i').text('bookmark')
    }else {
      $('#btn-favorite i').text('bookmark_border')
    }
  })

  $('#btn-toggleNight').click(function(){
    if ($('#btn-toggleNight').is(":checked")) {
      $('#filter-light').css('display', 'block')
    }else {
      $('#filter-light').css('display', 'none')
    }
  })

  $('#btn-zoom-in').click(function(){
    doZoomIn()
  })

  $('#btn-zoom-out').click(function(){
    doZoomOut()
  })

  $('#btn-default-level').click(function(){
    doDefaultLevel()
  })

  $('#btn-fullscreen').click(function(){
    doFullscreen()
  })

  $('#find-box').keyup(function(){
    // console.log($(this).val());
    var currentTabIndex = $('.active').index();
    var webview = $('.webview').find('webview')[currentTabIndex];
    if ($(this).val() !== ''){
      webview.findInPage($(this).val(), {forward: true, backward: false});
    }else {
      webview.stopFindInPage('clearSelection');
    }
  })
});
