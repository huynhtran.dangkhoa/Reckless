function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

var code = "";
var start_page = (__dirname + '/start-page.html').replace(/\\/g, "/");

var zoomLevels = [-3.75, -3.35, -2.5, -1.65, -1.25, -0.5, -0.25, 0, 0.25, 0.5, 1.25, 2.5, 3.75, 5, 7.5, 10, 15, 20];

function setPermission(bool){
  var webview = $('.webview').find('webview')[$('.active').index()]
  webview.getWebContents().session.setPermissionRequestHandler((webContents, permission, callback) => {
    // console.log(webContents.getURL());
    // callback(true)
    callback(bool)

    // callback(true)
  })
  webview.reload()
}

function didFinishLoad() {
  var currentTabIndex = $('.active').index()
  var webview = $('.webview').find('webview')[currentTabIndex]

  var wviews = $('webview');
  var v;
  for (v in wviews) {
    if (wviews.hasOwnProperty(v)) {
      var favicon_id = '#favicon'+$(wviews[v]).attr('id')

      var url;
      try {
        url = extractDomain(wviews[v].getURL());
        console.log(url);
      } catch (e) {

      }

      if (url != ""){
        var favicon_url = "https://www.google.com/s2/favicons?domain=" + url;
        $(favicon_id).attr('src', favicon_url);
      }

      function extractDomain(url) {
        var domain;
        //find & remove protocol (http, ftp, etc.) and get domain
        if (url.indexOf("://") > -1) {
            domain = url.split('/')[2];
        }
        else {
            domain = url.split('/')[0];
        }

        //find & remove port number
        domain = domain.split(':')[0];

        return domain;
      }
    }
  }
}

function didFrameFinishLoad() {
  var currentTabIndex = $('.active').index()
  var webview = $('.webview').find('webview')[currentTabIndex]
  // var lstTitles = JSON.parse(sessionStorage.getItem('lstTitles'))

  // console.log($('.webview'));
  var wviews = $('webview');
  var v;
  for (v in wviews) {
    if (wviews.hasOwnProperty(v)) {
      var tab_id = '#'+$(wviews[v]).attr('id')
      var title_id = '#title'+$(wviews[v]).attr('id')
      var favicon_id = '#favicon'+$(wviews[v]).attr('id')

      try {
        var title = $(wviews[v])[0].getTitle();
        var url = $(wviews[v])[0].getURL();

        if (url.indexOf(start_page) != -1){
          $(title_id).text('New tab')
        }else {
          if ($(title_id).text() == 'Loading...' || $(title_id).text() == 'New tab'){
            if ($(title_id).text() != title){
              $(title_id).text(title)
              $(tab_id).attr('title', title)
              console.log($(wviews[v])[0].getTitle());
              // console.log($(wviews[v])[0].getURL());
            }
          }
        }
      } catch (e) {

      }
    }
  }

  var url = webview.getURL();

  if (url.indexOf(start_page) != -1){
    $('#address').val("");
  }else {
    if ($('#address').val() != url){
      $('#address').val(url);
    }
  }

  if (webview.canGoBack()){
    $('#btn-back').removeClass('disabled')
    menu1.items[0].enabled = true;
  }else {
    $('#btn-back').addClass('disabled')
    menu1.items[0].enabled = false;
  }

  if (webview.canGoForward()){
    $('#btn-forward').removeClass('disabled')
    menu1.items[1].enabled = true;
  }else {
    $('#btn-forward').addClass('disabled')
    menu1.items[1].enabled = false;
  }

  $('#btn-refresh i').text('refresh');

  $('img').css('display', 'initial');
  $('.spinner').css('display', 'none');

  console.log("session: ", webview.getWebContents().session);

  webview.getWebContents().session.setPermissionRequestHandler((webContents, permission, callback) => {
    // console.log(webContents.getURL());
    if (webContents.getURL() == 'file:///home/dangkhoa/Desktop/Reckless/views/index.html'){
      callback(true);
    }
    console.log(permission);
    // callback(true)
  })



  // const {remote} = require('electron');
  // const {BrowserWindow} = remote;
  // const win = BrowserWindow.getFocusedWindow();
  //
  // win.webContents.session.removeAllListeners();
  // win.webContents.session.on('will-download', function(event, item, webContents){
  //   item.once('done', (event, state) => {
  //     console.log('Total: ', item.getTotalBytes());
  //   })
  // })
}

function didStartLoading() {
  var webview = $('.webview').find('webview')[$('.active').index()]

  if ($('.active div span').text() != 'Loading...' && $('.active div span').text() != 'New tab'){
    $('.active div span').text("Loading...")
  }

  $('#address').text(webview.src);

  if (webview.canGoBack()){
    $('#btn-back').removeClass('disabled')
    menu1.items[0].enabled = true;
  }else {
    $('#btn-back').addClass('disabled')
    menu1.items[0].enabled = false;
  }

  if (webview.canGoForward()){
    $('#btn-forward').removeClass('disabled')
    menu1.items[1].enabled = true;
  }else {
    $('#btn-forward').addClass('disabled')
    menu1.items[1].enabled = false;
  }

  $('#btn-refresh i').text('close');

  $('.active div img').css('display', 'none');
  $('.active div .spinner').css('display', 'initial');
}

function didStopLoading() {
  $('#btn-refresh i').text('refresh');
}

function didDomReady() {
  var webview = $('.webview').find('webview')[$('.active').index()]
  webview.setUserAgent(navigator.userAgent);

  var currentZoomLevel = localStorage.getItem('zoomLevel');
  webview.setLayoutZoomLevelLimits(0, parseFloat(zoomLevels[currentZoomLevel]));
  webview.setZoomLevel(parseFloat(100 + 20 * zoomLevels[currentZoomLevel]));
  $('#btn-default-level').text(parseInt(100 + 20 * zoomLevels[currentZoomLevel])+'%');

  // const {remote} = require('electron');
  // const {ipcRenderer} = remote;
  // const win = remote.getCurrentWindow();

  const {ipcRenderer} = require('electron')
  console.log(ipcRenderer.sendSync('synchronous-message', 'ping')) // prints "pong"

  //
  // win.webContents.session.on('will-download', function(event, item, webContents){
  //   item.on('updated', (event, state) => {
  //     // if (state === 'interrupted') {
  //     //   console.log('Download is interrupted but can be resumed')
  //     // } else if (state === 'progressing') {
  //     //   if (item.isPaused()) {
  //     //     console.log('Download is paused')
  //     //   } else {
  //     //     console.log(`Received bytes: ${item.getReceivedBytes()}`)
  //     //   }
  //     // }
  //     console.log('state updated: ', state);
  //     console.log(item.getReceivedBytes());
  //   })
  // })

  // const {remote} = require('electron');
  // const {session} = remote

  // session.defaultSession.removeAllListeners();
  // session.defaultSession.on('will-download', function(event, item, webContents){
  //   console.log("Download item: ", item);
  //   item.once('done', function(event, state) {
  //     // if (state == 'completed'){
  //     //
  //     // }
  //     console.log('Received bytes: ', item.getReceivedBytes())
  //     console.log(state);
  //   })
  // })

  // webview.getWebContents().session.setMaxListeners(0);
  // // console.log(webview.getWebContents().session.getMaxListeners());
  // webview.getWebContents().session.removeAllListeners();
  // // console.log(webview.getWebContents().session.listeners());
  // webview.getWebContents().session.on('will-download', function(event, item, webContents){
  //   item.on('updated', (event, state) => {
  //     // if (state === 'interrupted') {
  //     //   console.log('Download is interrupted but can be resumed')
  //     // } else if (state === 'progressing') {
  //     //   if (item.isPaused()) {
  //     //     console.log('Download is paused')
  //     //   } else {
  //     //     console.log('Received bytes: ', item.getReceivedBytes())
  //     //   }
  //     // }
  //   })
  //   console.log(item);
  //   item.once('done', (event, state) => {
  //     // if (state == 'completed'){
  //     //
  //     // }
  //     console.log(state);
  //   })
  // })

  webview.getWebContents().executeJavaScript("document.addEventListener('contextmenu', (e) => {if (e.target.localName != 'video'){localStorage.setItem('target', e.target.currentSrc);}else{localStorage.setItem('target', 'video');} }, false);", true, function(result) {

  })

  $(webview).focus(function(){
    $('.address-bar').removeClass('open')
    localStorage.setItem('zoomLevel', sessionStorage.getItem('tempZoomLevel'))
  })
}

function didNewWindow(url) {
  addTab(url)
}

function addTab(url) {
  $('#address').val("");

  if (url === undefined){
    url = '../views/start-page.html'
  }

  // $('#address').val('')
  var id = guid();

  $('.tab-container .nav-tabs').append('<li id="'+id+'" data-toggle="tooltip" data-placement="right" title="New tab"><div onclick="changeTab()"><img class="favicon" id="favicon'+id+'"/><svg class="spinner" viewBox="0 0 50 50"><circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="5"></circle></svg><span id="title'+id+'">New tab</span><i class="btn-close material-icons" onclick="closeTab()" data-toggle="tooltip" data-placement="right" title="Close tab">close</i></div></li>')

  //Remove active tab and hide webview
  $($('.webview').find('webview')[$('.active').index()]).css('height', 0)
  $('.active').removeClass('active')

  //Set new tab is active
  setTimeout(function () {
    var last = $('.tab-container .nav-tabs li').length - 1;
    $($('.tab-container .nav-tabs li')[last]).addClass('active')
  }, 100);

  //Append new webview
  setTimeout(function () {
    $('.webview').append('<webview id="'+id+'" src="'+url+'" useragent="Mozilla/5.0 (X11; Linux x86_64, Windows x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Windows Chromium/53.0.2785.143 Chrome/53.0.2785.143 Safari/537.36"/>')

    changeTab()

    //Add events for webview
    $('webview').on('did-finish-load',  function(){
      didFinishLoad()
    })
    $('webview').on('did-frame-finish-load',  function(){
      didFrameFinishLoad()
    })
    $('webview').on('did-start-loading',  function(){
      didStartLoading()
    })
    $('webview').on('did-stop-loading',  function(){
      didStopLoading()
    })
    $('webview').on('dom-ready',  function(){
      didDomReady()
    })
    var currentTabIndex = $('.active').index()
    console.log(currentTabIndex);
    var webview = $('.webview').find('webview')[currentTabIndex];
    webview.addEventListener('new-window',  function(r){
      didNewWindow(r.url)
    })
    createContextMenu()

    setTimeout(function () {
      $('#address').select()
    }, 120);
  }, 100);


}

function changeTab() {
  setTimeout(function () {
    $('.tab-container .nav-tabs li').unbind();
    var current = $('.active').index();
    console.log("current: ",current);

    $('.tab-container .nav-tabs li').click(function(){
      if ($(this).index() != current){
        //Remove active tab
        var listWebView = $('.webview webview');
        for (var i = 0; i < listWebView.length; i++) {
          $(listWebView[i]).css('height', 0)
        }

        var currentZoomLevel = localStorage.getItem('zoomLevel');
        webview.setZoomLevel(parseFloat(zoomLevels[currentZoomLevel]));
        $('#btn-default-level').text(parseInt(zoomLevels[currentZoomLevel]*100)+'%');

        $('.active').removeClass('active')

        //Set selected tab is active
        $(this).addClass('active')
        var id = '#'+$('.active').attr('id');
        $('.webview').find(id).css('height', '100%');

        if ($('.webview').find(id)[0].canGoBack()){
          $('#btn-back').removeClass('disabled')
          menu1.items[0].enabled = true;
        }else {
          $('#btn-back').addClass('disabled')
          menu1.items[0].enabled = false;
        }

        if ($('.webview').find(id)[0].canGoForward()){
          $('#btn-forward').removeClass('disabled')
          menu1.items[1].enabled = true;
        }else {
          $('#btn-forward').addClass('disabled')
          menu1.items[1].enabled = false;
        }
        // $('#address').val($('.webview').find(id)[0].getURL());

        if ($('.webview').find(id)[0].getURL().indexOf(start_page) != -1){
          $('#address').val("");
        }else {
          $('#address').val($('.webview').find(id)[0].getURL());
        }
      }
    });
  }, 200);
}

function closeTab(i, mode) {
  $('.tab-container .nav-tabs li').unbind();

  if (mode == "new-window"){
    var id = '#'+i;

    if ($(id).index() == $('.nav-tabs li').length-1){
      $($('.nav-tabs li')[$(id).index()-1]).addClass('active');
    }else {
      $($('.nav-tabs li')[$(id).index()+1]).addClass('active');
    }

    $(id).remove();
    $('.webview').find(id).remove();

    var idActive = '#'+$('.active').attr('id');

    if ($('.webview').find(idActive)[0].getURL().indexOf(start_page) != -1){
      $('#address').val("");
    }else {
      $('#address').val($('.webview').find(idActive)[0].getURL());
    }

    $('.webview').find(idActive).css('height', '100%');
  }else {
    if (i === undefined){
      if ($('.tab-container .nav-tabs li').length-1 > 0){
        $('.tab-container .nav-tabs li').click(function(){
          i = $(this).index();

          if ($('.active').index() == i){
            if ($('.active').index() < $('.nav-tabs li').length-1){
              $($('.nav-tabs li')[i+1]).addClass('active');
            }else {
              $($('.nav-tabs li')[i-1]).addClass('active');
            }
            setTimeout(function () {
              var idActive = '#'+$('.active').attr('id');
              $('.webview').find(idActive).css('height', '100%')
            }, 100);
          }

          var id = '#'+$(this).attr('id');
          $(this).remove()
          $('.webview').find(id).remove();

          var idActive = '#'+$('.active').attr('id');

          if ($('.webview').find(idActive)[0].getURL().indexOf(start_page) != -1){
            $('#address').val("");
          }else {
            $('#address').val($('.webview').find(idActive)[0].getURL());
          }
        })
      }else {
        console.log('close window');
        // window.close();
      }
    }else {
      var idActive = $('.active').attr('id');
      if (i != idActive){
        var id = '#'+i;
        $('.nav-tabs').find(id).remove();
        $('.webview').find(id).remove();
      }else {
        var index = $('.active').index()+1
        var id = '#'+$($('.nav-tabs li')[index]).attr('id');
        $('.nav-tabs').find(id).remove();
        $('.webview').find(id).remove();
      }
    }
  }


}

function cleanTabs() {
  var tabsLength = $('.tab-container .nav-tabs li').length;

  for (var i = 0; i < tabsLength-1; i++) {
    $('.tab-container .nav-tabs li').unbind()
    var id = $($('.tab-container .nav-tabs li')[0]).attr('id')
    closeTab(id)
  }
}

function checkUrl(url) {
  return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}

function goBack() {
  var currentTabIndex = $('.active').index();
  var webview = $('.webview').find('webview')[currentTabIndex];

  if (!$('#btn-back').hasClass('disabled')){
    if (webview.canGoBack()){
      webview.goBack()
    }
  }
}

function goForward() {
  var currentTabIndex = $('.active').index();
  var webview = $('.webview').find('webview')[currentTabIndex];

  if (!$('#btn-forward').hasClass('disabled')){
    if (webview.canGoForward()){
      webview.goForward()
    }
  }
}

function goHome() {
  var currentTabIndex = $('.active').index();
  var webview = $('.webview').find('webview')[currentTabIndex];
  webview.loadURL("http://google.com")
}

function doRefresh() {
  var currentTabIndex = $('.active').index();
  var webview = $('.webview').find('webview')[currentTabIndex];

  if (webview.isLoading()){
    webview.stop();
  }else {
    webview.reload();
  }
}

function doZoomIn() {
  var currentTabIndex = $('.active').index();
  var webview = $('.webview').find('webview')[currentTabIndex];

  var currentZoomLevel = parseInt(sessionStorage.getItem('tempZoomLevel'));

  if (currentZoomLevel >= 0 && currentZoomLevel < zoomLevels.length-1){
    currentZoomLevel += 1;
    sessionStorage.setItem('tempZoomLevel', currentZoomLevel);

    webview.setLayoutZoomLevelLimits(0, parseFloat(zoomLevels[currentZoomLevel]));
    webview.setZoomLevel(parseFloat(100 + 20 * zoomLevels[currentZoomLevel]));
    $('#btn-default-level').text(parseInt(100 + 20 * zoomLevels[currentZoomLevel])+'%');
  }
}

function doZoomOut() {
  var currentTabIndex = $('.active').index();
  var webview = $('.webview').find('webview')[currentTabIndex];

  var currentZoomLevel = parseInt(sessionStorage.getItem('tempZoomLevel'));

  if (currentZoomLevel > 0 && currentZoomLevel < zoomLevels.length){
    currentZoomLevel -= 1;
    sessionStorage.setItem('tempZoomLevel', currentZoomLevel);

    webview.setLayoutZoomLevelLimits(0, parseFloat(zoomLevels[currentZoomLevel]));
    webview.setZoomLevel(parseFloat(100 + 20 * zoomLevels[currentZoomLevel]));
    $('#btn-default-level').text(parseInt(100 + 20 * zoomLevels[currentZoomLevel])+'%');
  }else {
    console.log(currentZoomLevel);
  }
}

function doDefaultLevel() {
  var currentZoomLevel = parseInt(localStorage.getItem('zoomLevel'));
  webview.setLayoutZoomLevelLimits(0, parseFloat(zoomLevels[currentZoomLevel]));
  webview.setZoomLevel(parseFloat(100 + 20 * zoomLevels[currentZoomLevel]));
  sessionStorage.setItem('tempZoomLevel', currentZoomLevel);
  $('#btn-default-level').text(parseInt(100 + 20 * zoomLevels[currentZoomLevel])+'%');
}

function doFullscreen() {
  document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
}

function openDevTools(x, y) {
  console.log(x +  " " + y);

  var currentTabIndex = $('.active').index()
  var webview = $('.webview').find('webview')[currentTabIndex]

  webview.inspectElement(x, y-79);
}

function viewPageSource() {
  var currentTabIndex = $('.active').index();
  var webview = $('.webview').find('webview')[currentTabIndex];
  webview.getWebContents().executeJavaScript("result = document.documentElement.outerHTML", true, (result)=> {
    code = result;

    var fs = require('fs');
    var fn = __dirname + "/cv.html";

    fs.writeFile(fn, '<!DOCTYPE html><html><head><meta charset="utf-8"><title>view-soure</title><link rel="stylesheet" href="../css/cv.css"><title></title></head><body><div class=""><pre><ol>')

    fs.writeFile("temp.html", result, function(err) {
      if(err) {
          return console.log(err);
      }

      var readline = require('linebyline'), rl = readline("temp.html");

      rl.on('line', function(line) {
        line = line.replace(/</g, '&lt;');
        line = line.replace(/</g, '&gt;');

        var str = '<li>'+line+'</li>'

        fs.appendFile(fn, str, function(err) {
          if(err) {
              return console.log(err);
          }
        });
      })
      .on('end', function(e) {
        // something went wrong
        fs.appendFile(fn, '</ol></pre></div></body></html>', function(err) {
          if(err) {
              return console.log(err);
          }
          addTab('../views/cv.html')
        });
      });
    });
  });
}

function savePageAs(url) {
  const {remote} = require('electron');
  const {dialog, app} = remote;

  var currentTabIndex = $('.active').index();
  var webview = $('.webview').find('webview')[currentTabIndex];

  if (url === undefined){
    url = webview.getURL();
  }

  // var url = webview.getURL();
  // var url = 'https://static.pexels.com/photos/1380/black-and-white-city-skyline-buildings.jpg'
  var filename = webview.getTitle()+'.html';

  dialog.showSaveDialog({title: 'Save as...', defaultPath: app.getPath('downloads')+'/'+filename}, function(s){
    console.log(s)
    if (s != undefined){
      request(url).on('response', function(res){
        console.log(res);
        res.pipe(require('fs').createWriteStream(s))
      })
    }
  })
}

function openInNewTab() {
  addTab(target_link);
}

function openInNewWindow() {
  if (target_link != ''){
    localStorage.setItem('link_newWindow', target_link);
  }

  const {remote} = require('electron');
  const {BrowserWindow, ipcRenderer} = remote;
  const path = require('path')
  const url = require('url')
  let win = new BrowserWindow({
    width: 800,
    height: 600
  });

  win.maximize();
  // win.setMenu(null);
  // win.webContents.openDevTools();

  win.webContents.executeJavaScript('if (localStorage.getItem("link_newWindow")){document.getElementsByTagName("webview")[$(".active").index()].loadURL(localStorage.getItem("link_newWindow")); localStorage.removeItem("link_newWindow");}else{document.getElementsByTagName("webview")[$(".active").index()].loadURL("../views/start-page.html");}')

  win.loadURL(url.format({
    pathname: path.join(__dirname, '/index.html'),
    protocol: 'file:',
    slashes: true
  }));
}

function openImageInNewTab() {
  addTab(img_link);
}

function copyImageAddress() {
  const {clipboard} = require('electron')
  clipboard.writeText(img_link)
}

function copyLinkAddress() {
  const {clipboard} = require('electron')
  clipboard.writeText(target_link)
}

function openFindBox() {
  $('.find-box').css('display', 'flex');
  $('#find-box').focus()
}

function findInPage(mode) {
  var currentTabIndex = $('.active').index();
  var webview = $('.webview').find('webview')[currentTabIndex];

  if (mode === 'backward'){
    webview.findInPage($('#find-box').val(), {forward: false, backward: true});
  }else {
    webview.findInPage($('#find-box').val(), {forward: true, backward: false});
  }
}

function stopFindInPage() {
  var currentTabIndex = $('.active').index();
  var webview = $('.webview').find('webview')[currentTabIndex];

  $('.find-box').css('display', 'none');
  webview.stopFindInPage('clearSelection');
}
