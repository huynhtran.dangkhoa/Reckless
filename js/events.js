var wv = $('.webview').find('webview')[$('.active').index()]
// console.log(webview);

wv.addEventListener('did-finish-load', function(){
  didFinishLoad();
})

wv.addEventListener('did-frame-finish-load', function(){
  didFrameFinishLoad();
})

wv.addEventListener('did-start-loading', function(){
  didStartLoading();
});

wv.addEventListener('did-stop-loading',  function(){
  didStopLoading()
})

wv.addEventListener('dom-ready', function(){
  didDomReady()
})

wv.addEventListener('new-window', function(r){
  console.log(r);
  didNewWindow(r.url)
})
